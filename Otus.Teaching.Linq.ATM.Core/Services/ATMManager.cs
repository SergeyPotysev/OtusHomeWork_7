﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        public IEnumerable<User> Users { get; private set; }
        public IEnumerable<OperationsHistory> History { get; private set; }
        private string _login;
        private string _password;

        private delegate bool CheckUserData();


        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }


        //TODO: Добавить методы получения данных для банкомата
        public User GetUser()
        {
            IEnumerable<User> userQuery =
                from user in Users
                where user.Login == _login && user.Password == _password
                select user;
            return userQuery.Single();
        }


        public List<Account> GetAccount()
        {
            return Accounts.Where(x => x.UserId == Users.Where(x => x.Login.Equals(_login))
                                                        .Where(x => x.Password.Equals(_password))
                                                        .Select(x => x.Id).Single()).ToList();
        }


        public List<OperationsHistory> GetHistory(int id)
        {
            return History.Where(x => x.AccountId == id).ToList();
        }


        public List<OperationsHistory> GetInputCash()
        {
            var input = History.Where(x => x.OperationType == OperationType.InputCash).ToList();
            return input;
        }


        public string GetAccountOwner(int accountId)
        {
            int userId = Accounts.Where(x => x.Id == accountId).Select(x => x.UserId).Single();            
            return GetOwnerName(userId);
        }


        public string GetOwnerName(int userId)
        {
            return Users.Where(x => x.Id == userId).Select(x => x.FirstName + " " + x.SurName).Single();
        }


        public List<Account> GetAccountCash(decimal sum)
        {
            return Accounts.Where(x => x.CashAll > sum).ToList();     
        }

        public bool Authorization()
        {
            Console.SetCursorPosition(0, 2);
            Console.WriteLine("Авторизуйтесь, пожалуйста ...");
            CheckUserData d = LoginExists;
            if (!UserInput("Логин", 4, ref _login, d))
                return false;
            d = PasswordMatchesLogin;
            if (!UserInput("Пароль", 5, ref _password, d))
                return false;
            return true;
        }

        private bool UserInput(string fieldName, int strNum, ref string fieldValue, CheckUserData del)
        {
            Console.SetCursorPosition(0, strNum);
            Console.Write($"{fieldName}: ");
            int i = 3;
            while (i > 0)
            {
                Console.SetCursorPosition(8, strNum);
                fieldValue = Console.ReadLine().Trim();
                if (del())
                    break;
                --i;
                Console.SetCursorPosition(32, 2);
                Console.WriteLine($"Неверный {fieldName}! Осталось попыток: {i}");
                CleanString(8, strNum);
            }
            if (i == 0)
            {
                Console.WriteLine("\n\n\u263A Аккаунт заблокирован! Обратитесь в ближайшее отделение банка ...\n");
                return false;
            }
            CleanString(32, 2);
            Console.SetCursorPosition(32, 2);
            Console.Write($"{fieldName} указан верно!");
            return true;
        }

        public static void CleanString(int startPos, int strNum)
        {
            for (int n = startPos; n < Console.LargestWindowWidth; n++)
            {
                Console.SetCursorPosition(n, strNum);
                Console.Write(" ");
            }
        }

        private bool LoginExists()
        {
            // Поиск введенного логина в базе без учета регистра  
            bool result = Users.Any(x => string.Compare(x.Login, _login, true) == 0);
            if (result)
            {
                // Получить точное написание логина из базы с учетом регистра
                _login = Users.Where(x => string.Compare(x.Login, _login, true) == 0).Select(x => x.Login).Single();
            }
            else
                _login = string.Empty;
            return result;
        }

        private bool PasswordMatchesLogin()
        {
            // Поиск пары логин/пароль   
            return Users.Where(x => x.Login.Equals(_login)).Any(x => x.Password.Equals(_password));
        }
    }
}