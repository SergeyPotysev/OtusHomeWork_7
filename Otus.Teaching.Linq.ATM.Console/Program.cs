﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Console
{
    internal class Program
    {        
        private static void Main(string[] args)
        {
            int menu;
            string str;
            List<Account> accounts;
            System.Console.OutputEncoding = Encoding.UTF8;
            System.Console.WindowHeight = System.Console.LargestWindowHeight;
            System.Console.WindowWidth = System.Console.LargestWindowWidth;
            System.Console.SetBufferSize(System.Console.WindowWidth, System.Console.WindowHeight);
            System.Console.SetWindowPosition(0, 0);
            System.Console.WriteLine("Старт приложения-банкомата...");
            ATMManager atmManager = CreateATMManager();
            // Авторизация
            if (atmManager.Authorization())
            {
                User user = atmManager.GetUser();
                string userName = atmManager.GetOwnerName(user.Id);
                CleanScrean(userName);
                System.Console.WriteLine("\nВыберите номер пункта меню для продолжения работы ...");
                while (true)
                {
                    menu = ConsoleKey();
                    CleanScrean(userName);
                    System.Console.SetCursorPosition(0, 9);
                    System.Console.WriteLine($"Выбран пункт меню: {menu}\n");
                    switch (menu)
                    {
                        case 1:
                            // Вывод информации о заданном аккаунте по логину и паролю
                            System.Console.WriteLine("Персональные данные пользователя:\n");
                            foreach (PropertyInfo prop in user.GetType().GetProperties())
                                System.Console.WriteLine("{0,-25}{1}", prop.Name, prop.GetValue(user, null));
                            System.Console.WriteLine();
                            break;
                        case 2:
                            // Вывод данных о всех счетах заданного пользователя
                            accounts = atmManager.GetAccount();
                            System.Console.WriteLine("Счета пользователя:\n");
                            foreach (Account acc in accounts)
                            {
                                foreach (PropertyInfo prop in acc.GetType().GetProperties())
                                {
                                    System.Console.WriteLine("{0,-25}{1}", prop.Name, prop.GetValue(acc, null));
                                }
                                System.Console.WriteLine();
                            }
                            break;
                        case 3:
                            // Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту                            
                            accounts = atmManager.GetAccount();
                            System.Console.WriteLine("Счета пользователя с историей работы:\n");
                            foreach (Account acc in accounts)
                            {
                                foreach (PropertyInfo prop in acc.GetType().GetProperties())
                                {
                                    System.Console.WriteLine("{0,-25}{1}", prop.Name, prop.GetValue(acc, null));
                                }
                                System.Console.WriteLine();
                                List<OperationsHistory> history = atmManager.GetHistory(acc.Id);
                                foreach (OperationsHistory his in history)
                                {
                                    str = "\t";
                                    foreach (PropertyInfo prop in his.GetType().GetProperties())
                                    {
                                        str += $"{prop.Name} = {prop.GetValue(his, null)}, ";
                                    }                                        
                                    System.Console.WriteLine(str.Remove(str.Length - 2, 2));
                                }
                                System.Console.WriteLine();
                            }
                            break;
                        case 4:
                            // Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
                            List<OperationsHistory> inputCash = atmManager.GetInputCash();
                            System.Console.WriteLine("Операции пополнения счета с именем владельца:\n");
                            foreach (OperationsHistory input in inputCash)
                            {
                                var owner = atmManager.GetAccountOwner(input.AccountId);
                                str = string.Empty;
                                foreach (PropertyInfo prop in input.GetType().GetProperties())
                                {
                                    str += $"{prop.Name} = {prop.GetValue(input, null)}, ";
                                }
                                str += $"Владелец счета: {owner}";
                                System.Console.WriteLine(str);
                            }
                            System.Console.WriteLine();
                            break;
                        case 5:
                            // Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)
                            System.Console.WriteLine("Данные пользователей, имеющих на счету сумму больше указанной:\n");
                            System.Console.Write("Введите сумму: ");
                            decimal sum = ConsoleDec();
                            accounts = atmManager.GetAccountCash(sum);
                            foreach (Account acc in accounts)
                            {
                                var owner = atmManager.GetOwnerName(acc.UserId);
                                System.Console.WriteLine($"Владелец счета: {owner}, Номер счета: {acc.Id}, Сумма счета: {acc.CashAll}");
                            }
                            System.Console.WriteLine();
                            break;
                        case 6:
                            break;
                    }
                    if (menu == 6)
                        break;
                    else
                        System.Console.WriteLine("\nВыберите номер пункта меню для продолжения работы ...\n");
                }
            }
            System.Console.WriteLine("Завершение работы приложения-банкомата ...");
        }


        private static ATMManager CreateATMManager()
        {
            using ATMDataContext dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }


        private static int ConsoleKey()
        {
            char key;
            int result;
            while (true)
            {
                System.Console.SetCursorPosition(0, 8);
                key = System.Console.ReadKey().KeyChar;
                System.Console.SetCursorPosition(0, 8);
                System.Console.Write(" ");
                if (char.IsDigit(key))
                {
                    result = int.Parse(key.ToString());
                    if (result >= 1 && result <= 6)
                        return result;
                }
            }
        }


        private static decimal ConsoleDec()
        {
            decimal result = 0;
            while(true)
            {
                ATMManager.CleanString(15, 13);
                System.Console.SetCursorPosition(15, 13);
                string str = System.Console.ReadLine();
                string digitStr = new string(str.Where(t => char.IsDigit(t)).ToArray());
                if (digitStr.Length > 0)
                {
                    result = Convert.ToDecimal(digitStr);
                    ATMManager.CleanString(15, 13);
                    System.Console.SetCursorPosition(15, 13);
                    System.Console.WriteLine(result + "\n");
                    break;
                }
            }
            return result;
        }


        private static void CleanScrean(string userName)
        {
            System.Console.Clear();                
            System.Console.WriteLine($"Здравствуйте, {userName}! Для Вас есть специальное предложение!\n");
            System.Console.WriteLine("1. Вывод информации о аккаунте");
            System.Console.WriteLine("2. Вывести данные о всех счетах");
            System.Console.WriteLine("3. Вывод данных о всех счетах, включая историю по каждому счёту");
            System.Console.WriteLine("4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта");
            System.Console.WriteLine("5. Вывод данных о всех пользователях, у которых на счёте сумма больше указанной");
            System.Console.WriteLine("6. Завершение работы\n");             
        }
    }
}